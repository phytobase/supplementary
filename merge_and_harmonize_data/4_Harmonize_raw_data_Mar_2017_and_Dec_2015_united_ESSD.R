#### HARMONIZE TAXONOMIC NAMES IN THE UNITED RAW DATABASE 2015 PLUS 2017, USING A SPECIES SYNONYM TABLE

# Date: 2018-10-02
#
# Author: Damiano Righetti, rdamiano@ethz.ch
# Environmental Physics Group, Institute of Biogeochemistry
# and Pollutant Dynamics, ETH Zurich, CH

### ======================================================================
### Preparatory steps
### ======================================================================

# Clean home, defined directory, load libraries
rm(list = ls())
setwd("~/Desktop/PHYTO_DATA_ESSD") # Define name of the working directory
library(raster)
library(sfsmisc)
library(grid) 
library(spam)
library(fields)
library(base)
library(doBy)
library(doParallel)

# Get data on synonymous species (genus) names
tab <- read.csv ("./Synonym_table_total/Synonym_table_2018_09_28.csv") # Get the synonym table (species synonym table) containing all 2015 and 2017 dataset species (genera)
tab$scientificName_original <- as.character(tab$scientificName_original)
tab$scientificName_harmonized <- as.character(tab$scientificName_harmonized)

# Get the united observational data (note that 'taxon' means here 'original taxon name')
tot_raw_uni <- get(load('./Data_intermediate_steps/Species_obs_raw_united_2017_with_2015.RData'))
base <- tot_raw_uni # Create a copy of the dataset: for final extraction of information (column complementation) regarding IDs and Orgs of GBIF, OBIS, MAREDAT, SAl, VILLAR

# A quick glance at the observational data
head(tot_raw_uni)
dim(tot_raw_uni) # 1'594'649 x 35
sum(is.na(tot_raw_uni$depth)) # 83'477
sum(is.na(tot_raw_uni$depth))/nrow(tot_raw_uni) # 5.223% of data observation records have no depth value

### ======================================================================
### Harmonization: construct 'deep ocean dataset' (i.e., all species are considered that have at least one depth-referenced record, inside or below the oceanic mixed-layer, as defined by de Boyer Montegut, 2004)
### ======================================================================

### Step one - exclude species that have no single depth-referenced observation. These species were not considered for the species synonym table, and hence cannot be harmonized:
tot_raw_uni$taxon <- as.character(tot_raw_uni$taxon)
dat.split <- split(tot_raw_uni, f = tot_raw_uni$taxon)
is.na.depth <- vector()
for(i in 1:length(dat.split)){
data.species <- dat.split[[i]]
if(   is.na(mean(data.species$depth, na.rm=T)) == T   ){   is.na.depth[i] <- "exclude"   } else {   is.na.depth[i] <- "fine"   }
}
length(is.na.depth) # 4'741 species names
length(is.na.depth[which(is.na.depth=="exclude")]) # 1'441 excluded
length(is.na.depth[which(is.na.depth=="fine")]) # 3'300 retained
dat.split.newuni <- dat.split[which(is.na.depth=="fine")] # Exclude non-depth referenced species
tot_raw_uni_new <- do.call(rbind, dat.split.newuni) # Takes some 5'

### Step two - harmonization:
tot.raw.uni1 <- tot_raw_uni_new
tot.raw.uni1$scientificName_original <- tot.raw.uni1$taxon
tot.raw.uni1$scientificName_original <- as.character(tot.raw.uni1$scientificName_original)
tot.raw.uni1$taxon <- "NA" # Set taxon name to NA, serves as placeholder for the new (harmonized) taxon name

# Add Id for re-ordering the data
tot.raw.uni1$Id <- 1:nrow(tot.raw.uni1)

# Are all taxa names to be harmonized in the observational data contained in the synonym table?
length (unique(tot.raw.uni1$scientificName_original)) # 3300
sum(  unique(tot.raw.uni1$scientificName_original)%in%tab$scientificName_original) # OK, all contained
unique(tot.raw.uni1$scientificName_original)[which(unique(tot.raw.uni1$scientificName_original)%in%tab$scientificName_original == F)] # 0, QED

# Harmonization - loop across taxa to assign new names to original names - takes some 5 minutes
lisi.taxa <- list()
for (i in 1:length(unique (tot.raw.uni1$scientificName_original))){
dat.tax <- tot.raw.uni1 [  which  (  tot.raw.uni1$scientificName_original == unique (tot.raw.uni1$scientificName_original)[i]  )  ,  ]
dat.tax$taxon <- as.character(tab[ which( tab$scientificName_original == dat.tax$scientificName_original[1]), "scientificName_harmonized"])
dat.tax$flag_suitable_species <- as.character(tab[ which( tab$scientificName_original == dat.tax$scientificName_original[1]), "isSelectedForPhytoplanktonDatabase"])
lisi.taxa [[i]] <- dat.tax
print(paste(i))
}

# Merge data
tt.uni <- do.call(rbind, lisi.taxa) # Takes some 5'
tt.uni <- tt.uni[order(tt.uni$Id), ]
tt.uni$Id <- NULL
rownames(tt.uni) <- 1:nrow(tt.uni)
head(tt.uni)
dim(tt.uni) # 1'589'515

## Key operation, remove unsuitable (non-phytoplankton) species, and select extant and marine phytoplantkon species  
tt.unifinal <- tt.uni[tt.uni$flag_suitable_species == "YES", ]

## Detect different groups associated with the same species: correct for group consistency
df.unique <- as.data.frame(unique(tt.unifinal[ , c("taxon", "group")]))
df.unique[duplicated(df.unique$taxon),] # Tryblionella compressa
tt.unifinal[which(tt.unifinal$taxon=="Tryblionella compressa"), "group"] <- "bacillariophyceae" # assign "Tryblionella compressa" to group bacillariophyceae (not dinoflagellata), this is the reason why inconsistency with backbone key exists
#
## Save database version containing all original names (some of the new names are therefore duplicated)
dim(tt.unifinal) # 1'500'103 x 37
fln <- "./Data_intermediate_steps/Species_obs_harmonized_united_2017_with_2015_full_depth_(retaining_original_names).RData"
save(tt.unifinal, file = fln)

### CREATE NEW ID FOR EACH OBSERVATION POINT
#################################################
tt.har <-  get(load("./Data_intermediate_steps/Species_obs_harmonized_united_2017_with_2015_full_depth_(retaining_original_names).RData"))
tt.har$runID <- 1:nrow(tt.har)

# Column adjustments
class(tt.har$IndividualCount)
tt.har$IndividualCount[200:400]
tt.har$IndividualCount <- as.character(tt.har$IndividualCount)
class(tt.har$VolumeBasisOfIndividualCount)
class(tt.har$Cells_per_l)
tt.har$Cells_per_l[200:400]
tt.har$Cells_per_l <- as.character(tt.har$Cells_per_l)
class(tt.har$Colonial_form_cells_per_l)
tt.har$Colonial_form_cells_per_l <- as.character(tt.har$Colonial_form_cells_per_l)
class(tt.har$Total_colonial_or_single_cells_or_trichomes_l)
tt.har$Total_colonial_or_single_cells_or_trichomes_l <- as.character(tt.har$Total_colonial_or_single_cells_or_trichomes_l)

# Create ID.new, here the new taxon name becomes part of the ID of each observation point
tt.har$ID.original <- tt.har$ID
n.cores <- 7
cl = makeCluster(n.cores, outfile="") # outfile="" returns progress
registerDoParallel(cl) # cluster: results by default are returned in a list
lisi.IDs <- foreach (i = 1:nrow(tt.har), .packages = c('doBy')) %dopar% {
	ID <- paste(tt.har[i, c("taxon", "group", "longitude","latitude","year","month","day", "depth")], sep="", collapse="_")
	print(paste(i))	
	return(ID)
}
stopCluster(cl)
vec.IDs <- c(unlist(lisi.IDs))
head(vec.IDs)
tt.har$ID.new <- vec.IDs
dim(tt.har) # 1'500'103 x 40

# Check original vs. new taxon names
length(unique(tt.har$scientificName_original)) # 2'041
length(unique(tt.har$taxon)) # 1'717, i.e. 324 taxon names have been aggregated by taxonomic harmonization

######################### Intermediate save ###############################
fln <- "./Data_intermediate_steps/Species_obs_harmonized_united_2017_with_2015_full_depth_(retaining_original_names_adding_new_ID).RData"
save(tt.har, file = fln)
tt.har <- get(load("./Data_intermediate_steps/Species_obs_harmonized_united_2017_with_2015_full_depth_(retaining_original_names_adding_new_ID).RData"))
#######################################################################

# RETAIN META INFORMATION, WHEN REMOVING DUPLICATES, WITH EACH OBSERVATION POINT
# A) Changeable Parameters (using the function 'aggregate')
########################################################################

## PREPARE a column indicating the original version ("YearOfDataAccess) or dataset ("SourcheArchive") in tt.har, removing duplicated observation records by aggregating data to IDs (collapsing data in which IDs are duplicated, in case there are duplicates)
# Year of data assembly and source
vec.version <- aggregate(YearOfDataAccess~ID.new, data= tt.har, paste, collapse="_")
vec.sourcearchive <- aggregate(SourceArchive~ID.new, data= tt.har, paste, collapse="_")

## PREPARE abundance
vec.individcounts <- aggregate(IndividualCount~ID.new, data= tt.har, paste, collapse="_", na.action = na.pass)
vec.VolumeBasisOfIndividualCount <- aggregate(VolumeBasisOfIndividualCount~ID.new, data= tt.har, paste, collapse="_", na.action = na.pass)
vec.cellsperlitre <- aggregate(Cells_per_l~ID.new, data= tt.har, paste, collapse="_", na.action = na.pass)
vec.colonialcells <- aggregate(Colonial_form_cells_per_l~ID.new, data= tt.har, paste, collapse="_", na.action = na.pass)
vec.colonialorsinglecells <- aggregate(Total_colonial_or_single_cells_or_trichomes_l~ID.new, data=tt.har, paste, collapse="_", na.action = na.pass)

# YEAR AND SOURCE
####################

# IMPLEMENTATION 1: Remove duplicates in "tt.unifinal" regarding the species-point IDs (x,y,year,month,day,taxon,depth,group)
tt.har.new <- tt.har[!duplicated(tt.har$ID.new), ]
dim(tt.har.new) # 1'360'765 x 40

# IMPLEMENTATION 2: Add the "dowload version" data-field, collapse regarding observation IDs, add "download version" information (vec.version, see above), using the matching key == ID
tt <- merge( tt.har.new[,-which(names(tt.har.new)=="YearOfDataAccess")], vec.version, by ="ID.new")
unique(tt$YearOfDataAccess)
tt[which(tt$YearOfDataAccess == "2015_2017_2017"), "YearOfDataAccess"] <- "2015_2017"
tt[which(tt$YearOfDataAccess == "2015_2015_2017"), "YearOfDataAccess"] <- "2015_2017"
tt[which(tt$YearOfDataAccess ==  "2015_2017_2015"), "YearOfDataAccess"] <- "2015_2017"
tt[which(tt$YearOfDataAccess ==  "2017_2015"), "YearOfDataAccess"] <- "2015_2017"
tt[which(tt$YearOfDataAccess ==  "2015_2017_2015_2017"), "YearOfDataAccess"] <- "2015_2017"
tt[which(tt$YearOfDataAccess ==  "2017_2015_2017"), "YearOfDataAccess"] <- "2015_2017"
tt[which(tt$YearOfDataAccess ==  "2015_2015"), "YearOfDataAccess"] <- "2015"
tt[which(tt$YearOfDataAccess ==  "2017_2017"), "YearOfDataAccess"] <- "2017"
tt[which(tt$YearOfDataAccess ==  "2015_2017_2015_2015_2017"), "YearOfDataAccess"] <- "2015_2017"
tt[which(tt$YearOfDataAccess ==  "2017_2015_2017_2015"), "YearOfDataAccess"] <- "2015_2017"
tt[which(tt$YearOfDataAccess ==  "2015_2015_2017_2017"), "YearOfDataAccess"] <- "2015_2017"
tt[which(tt$YearOfDataAccess ==  "2015_2017_2017_2015"), "YearOfDataAccess"] <- "2015_2017"
tt[which(tt$YearOfDataAccess ==  "2017_2015_2015"), "YearOfDataAccess"] <- "2015_2017"
tt[which(tt$YearOfDataAccess ==  "2015_2015_2017_2015_2017"), "YearOfDataAccess"] <- "2015_2017"
tt[which(tt$YearOfDataAccess ==  "2015_2015_2015_2017"), "YearOfDataAccess"] <- "2015_2017"
tt[which(tt$YearOfDataAccess ==  "2017_2017_2017"), "YearOfDataAccess"] <- "2017"
tt[which(tt$YearOfDataAccess ==  "2015_2015_2017_2015_2015"), "YearOfDataAccess"] <- "2015_2017"
tt[which(tt$YearOfDataAccess ==  "2015_2017_2015_2017_2015_2015"), "YearOfDataAccess"] <- "2015_2017"
tt[which(tt$YearOfDataAccess ==  "2015_2017_2017_2015"), "YearOfDataAccess"] <- "2015_2017"
unique(tt$YearOfDataAccess)

# IMPLEMENTATION 3: Add the "SourceArchive" data-field, collapse data regarding observation IDs, add "SourceArchive" information
tt <- merge( tt[,-which(names(tt)=="SourceArchive")], vec.sourcearchive, by ="ID.new")
head(tt)
unique(tt$SourceArchive)
# adjustments: 
tt[which(tt$SourceArchive == "GB_GB"), "SourceArchive"] <- "GB"
tt[which(tt$SourceArchive == "GB_OB_OB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==  "GB_OB_GB_OB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==  "OB_OB"), "SourceArchive"] <- "OB"
tt[which(tt$SourceArchive ==  "MA_MA"), "SourceArchive"] <- "MA"
tt[which(tt$SourceArchive ==  "GB_OB_GB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==  "GB_GB"), "SourceArchive"] <- "GB"
tt[which(tt$SourceArchive ==  "OB_GB_OB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==  "OB_GB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==  "OB_GB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==  "GB_GB_OB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==  "OB_MA_MA"), "SourceArchive"] <- "OB_MA"
tt[which(tt$SourceArchive ==  "OB_MA_GB_MA"), "SourceArchive"] <- "GB_OB_MA"
tt[which(tt$SourceArchive ==  "GB_MA_MA"), "SourceArchive"] <- "GB_MA"
tt[which(tt$SourceArchive ==  "GB_MA_GB_MA"), "SourceArchive"] <- "GB_MA"
tt[which(tt$SourceArchive ==  "GB_OB_MA_MA"), "SourceArchive"] <- "GB_OB_MA"
tt[which(tt$SourceArchive ==   "GB_OB_MA_GB_MA"), "SourceArchive"] <- "GB_OB_MA"
tt[which(tt$SourceArchive ==   "OB_MA_OB_MA"), "SourceArchive"] <- "OB_MA"
tt[which(tt$SourceArchive ==   "VI_VI"), "SourceArchive"] <- "VI"
tt[which(tt$SourceArchive ==   "GB_OB_MA_GB_OB_MA"), "SourceArchive"] <- "GB_OB_MA"
tt[which(tt$SourceArchive ==   "MA_OB_MA"), "SourceArchive"] <- "OB_MA"
tt[which(tt$SourceArchive ==   "OB_GB_MA"), "SourceArchive"] <- "GB_OB_MA"
tt[which(tt$SourceArchive ==   "OB_GB_GB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==   "OB_GB_GB_OB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==   "MA_GB"), "SourceArchive"] <- "GB_MA"
tt[which(tt$SourceArchive ==   "SA_SA"), "SourceArchive"] <- "SA"
tt[which(tt$SourceArchive ==   "GB_OB_GB_MA"), "SourceArchive"] <- "GB_OB_MA"
tt[which(tt$SourceArchive ==   "OB_MA_GB"), "SourceArchive"] <- "GB_OB_MA"
tt[which(tt$SourceArchive ==   "GB_OB_MA_GB"), "SourceArchive"] <- "GB_OB_MA"
tt[which(tt$SourceArchive ==   "MA_OB"), "SourceArchive"] <- "OB_MA"
tt[which(tt$SourceArchive ==   "GB_GB_OB_GB_OB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==   "OB_OB_OB"), "SourceArchive"] <- "OB"
tt[which(tt$SourceArchive ==   "OB_GB_OB_GB_GB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==   "GB_GB_GB"), "SourceArchive"] <- "GB"
tt[which(tt$SourceArchive ==   "GB_OB_GB_OB_OB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==   "GB_OB_GB_OB_OB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==   "OB_GB_OB_GB_OB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==   "GB_OB_GB_OB_GB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==   "OB_GB_GB_OB_GB_OB"), "SourceArchive"] <- "GB_OB"
unique(tt$SourceArchive)

# IMPLEMENTATION 4A: Add the "IndividualCounts" data-field, collapse data regarding observation IDs, add "vec.individcounts" information, using the matching key == ID
dim(tt[which(tt$IndividualCount == "NA"),]) # 1'368'295 x 40
tt2 <- merge( tt[,-which(names(tt)=="IndividualCount")], vec.individcounts, by ="ID.new")
dim(tt2[which(tt2$IndividualCount == "NA"),]) # 1'031'265 x 40
# adjustments:
vv <- tt2$IndividualCount
vv[which(vv == "NA_NA")] <- "NA"
vvnew <- cbind(sapply(vv, FUN = function(x){x1 <- gsub ("NA_","",x); return(x1)}))
vvnewnew <- cbind(sapply(vvnew, FUN = function(x){x1 <- gsub ("_NA","",x); return(x1)}))
vvv <- data.frame(do.call("rbind", strsplit(as.character (vvnewnew),"_" ,fixed =T)  ))
tt2$IndividualCount <- as.character(vvv[ , 1])
tt2$IndividualCount <- as.numeric(as.character(tt2$IndividualCount))
unique(tt2$IndividualCount)

# IMPLEMENTATION 4B: Add the "VolumeBasisOfIndividualCount" data-field, collapse data regarding observation IDs, add "vec.VolumeBasisOfIndividualCount" information, using the matching key == ID
dim(tt2[which(tt2$VolumeBasisOfIndividualCount == "NA"),])
tt3 <- merge( tt2[,-which(names(tt2)=="VolumeBasisOfIndividualCount")], vec.VolumeBasisOfIndividualCount, by ="ID.new")
dim(tt3[which(tt3$VolumeBasisOfIndividualCount == "NA"),])
unique(tt3$VolumeBasisOfIndividualCount)
tt3[which(tt3$VolumeBasisOfIndividualCount == "NA_NA"), "VolumeBasisOfIndividualCount"] <- "NA"
tt3[which(tt3$VolumeBasisOfIndividualCount == "NA_1_litre"), "VolumeBasisOfIndividualCount"] <- "1_litre"
tt3[which(tt3$VolumeBasisOfIndividualCount == "NA_NA_NA"), "VolumeBasisOfIndividualCount"] <- "NA"
tt3[which(tt3$VolumeBasisOfIndividualCount == "NA_NA_NA"), "VolumeBasisOfIndividualCount"] <- "NA"
tt3[which(tt3$VolumeBasisOfIndividualCount == "1_litre_NA"), "VolumeBasisOfIndividualCount"] <- "1_litre"
tt3[which(tt3$VolumeBasisOfIndividualCount == "1_litre_1_litre"), "VolumeBasisOfIndividualCount"] <- "1_litre"
tt3[which(tt3$VolumeBasisOfIndividualCount == "NA_NA_NA_NA"), "VolumeBasisOfIndividualCount"] <- "NA"
unique(tt3$VolumeBasisOfIndividualCount)

# IMPLEMENTATION 4C: Add the "Cells_per_l" data-field, collapse data regarding observation IDs using the mean value, add "vec.cellsperlitre" information, using the matching key == ID
dim(tt3[which(tt3$Cells_per_l == "NA"),])
tt4 <- merge( tt3[,-which(names(tt3)=="Cells_per_l")], vec.cellsperlitre, by ="ID.new")
dim(tt4[which(tt4$Cells_per_l == "NA"),])
unique(tt4$Cells_per_l)
tt4[which(tt4$Cells_per_l  == "NA_NA_NA"), ] # one check
# adjustments:
vv <-tt4$Cells_per_l
vv[which(vv == "NA_NA")] <- "NA"
vv[which(vv == "NA_NA_NA")] <- "NA"
vv[which(vv == "NA_NA_NA_NA")] <- "NA"
unique(vv)
vvnew <- cbind(sapply(vv, FUN = function(x){x1 <- gsub ("NA_","",x); return(x1)}))
vvnew <- cbind(sapply(vvnew, FUN = function(x){x1 <- gsub ("_NA","",x); return(x1)}))
vu <- data.frame(do.call("rbind", strsplit(as.character (vvnew),"_" ,fixed =T)))
vu[ , 1] <- as.numeric(as.character(vu[,1]))
vu[ , 2] <- as.numeric(as.character(vu[,2]))
which(vu$X1 != vu$X2)
vu[37711, ]
vu[37711, "X1"] <- mean(c(100, 1040))
tt4$Cells_per_l <- vu$X1
unique(tt4$Cells_per_l)

# IMPLEMENTATION 4D: Add the "Colonial_form_cells_per_l" data-field, collapse data regarding observation IDs, add "vec.colonialcells" information, using the matching key == ID
dim(tt4[which(tt4$Colonial_form_cells_per_l == "NA"),]) # 1'359'801
tt5 <- merge( tt4[,-which(names(tt4)=="Colonial_form_cells_per_l")], vec.colonialcells, by ="ID.new")
dim(tt5[which(tt5$Colonial_form_cells_per_l == "NA"),])
unique(tt5$Colonial_form_cells_per_l)
# adjustments:
vv <-tt5$Colonial_form_cells_per_l
vv[which(vv == "NA_NA")] <- "NA"
vv[which(vv == "NA_NA_NA")] <- "NA"
vv[which(vv == "NA_NA_NA_NA")] <- "NA"
unique(vv)
tt5$Colonial_form_cells_per_l <- vv
tt5$Colonial_form_cells_per_l <- as.numeric(tt5$Colonial_form_cells_per_l)
unique(tt5$Colonial_form_cells_per_l)

# IMPLEMENTATION 4E: Add the "Total_colonial_or_single_cells_or_trichomes_l" data-field, collapse data regarding observation IDs, add "vec.colonialorsinglecells" information, using the matching key == ID
dim(tt5[which(tt5$Total_colonial_or_single_cells_or_trichomes_l == "NA"),]) # 1'357'699
tt6 <- merge( tt5[,-which(names(tt5)=="Total_colonial_or_single_cells_or_trichomes_l")], vec.colonialorsinglecells, by ="ID.new")
dim(tt6[which(tt6$Total_colonial_or_single_cells_or_trichomes_l == "NA"),])
unique(tt6$Total_colonial_or_single_cells_or_trichomes_l)
# adjustments:
vv <-tt6$Total_colonial_or_single_cells_or_trichomes_l
vv[which(vv == "NA_NA")] <- "NA"
vv[which(vv == "NA_NA_NA")] <- "NA"
vv[which(vv == "NA_NA_NA_NA")] <- "NA"
unique(vv)
tt6$Total_colonial_or_single_cells_or_trichomes_l <- vv
tt6$Total_colonial_or_single_cells_or_trichomes_l <- as.numeric(tt6$Total_colonial_or_single_cells_or_trichomes_l)
unique(tt6$Total_colonial_or_single_cells_or_trichomes_l)
head(tt6)

# Order
tt <- tt6
tt <- tt[order(tt$runID),]
tt$runID <- NULL

# Adjust rownames
rownames(tt) <- 1:nrow(tt)
dim(tt) # 1'360'765 x 39

# ADD DATASETKEYS AND INFORMATION ON THE ORIGINAL ORGANIZATION OF COLLECTION TO RECORDS: 
# B) Fixed Parameters (using the function 'match') 
# i.e., Add information to each record with respect to their original source GBIF, OBIS, MAREDAT, Sal et al. 2013, Villar et al. 2015)
########################################################################################

# Get original information: full length version of data
thekeys <- get(load('./Data_intermediate_steps/total_keys.RData'))

# Test: are all new IDs of tt contained in "thekeys". Answer: almost. We miss 65 positions. Fine.
length(tt$ID.new[tt$ID.new%in%thekeys$ID.new == F]) # 65 positions are missing - reason unclear. For species: Tryblionella compressa
head(tt[tt$ID.new%in%thekeys$ID.new == F, ])

# Incept the original group name into the new ID for those records ( T. compressa was previously assigned to Dinoflagellata for these records, not Bacillariophyceae)
tt$ID.new.final <- tt$ID.new
IDadjusted <- gsub("bacillariophyceae", "dinoflagellata", tt[tt$ID.new%in%thekeys$ID.new == F, ]$ID.new)
tt[ tt$ID.new%in%thekeys$ID.new == F , "ID.new"] <- IDadjusted

# Re-test: are all new IDs of tt contained in "thekeys". Answer: yes
length(tt$ID.new[tt$ID.new%in%thekeys$ID.new == F]) # 0, QED

# Gbif keys and orgs
unique(tt$SourceArchive)
RecordIDs.for.which.we.want.gbif.information <- tt[ which(tt$SourceArchive%in%c("GB", "GB_OB", "GB_MA", "GB_OB_MA")), "ID.new"] # Define observations in the merged dataset of interest
GB_keys_and_orgs <- thekeys[ which(thekeys$ID.new%in%RecordIDs.for.which.we.want.gbif.information & thekeys$SourceArchive%in%c("GB")), c("ID.new", "DatasetKey_gbif", "PublishingOrgKey_gbif", "InstitutionCode_gbif", "SourceArchive")] # Get information from the non-merged data set for these observations
tt$DatasetKey_gbif <- GB_keys_and_orgs$DatasetKey_gbif[match(tt$ID.new, GB_keys_and_orgs$ID.new)] # Add this information to each record
tt$PublishingOrgKey_gbif <- GB_keys_and_orgs$PublishingOrgKey_gbif[match(tt$ID.new, GB_keys_and_orgs$ID.new)] # Add this information to each record
tt$InstitutionCode_gbif <- GB_keys_and_orgs$InstitutionCode_gbif[match(tt$ID.new, GB_keys_and_orgs$ID.new)] # Add this information to each record
head(tt[which(tt$SourceArchive=="GB_OB"),])

# Obis keys and orgs
RecordIDs.for.which.we.want.obis.information <- tt[ which(tt$SourceArchive%in%c("GB_OB",  "OB" ,  "OB_MA",  "GB_OB_MA")), "ID.new"] # Define observations in the merged dataset of interest
length(RecordIDs.for.which.we.want.obis.information)
OB_keys_and_orgs <- thekeys[ which(thekeys$ID.new%in%RecordIDs.for.which.we.want.obis.information & thekeys$SourceArchive%in%c("OB")), c("ID.new", "CollectionCode_obis", "InstitutionCode_obis",  "Resname_obis", "ResourceID_obis", "SourceArchive") ] # Get information from the non-merged data set for these observations
tt$CollectionCode_obis <- OB_keys_and_orgs$CollectionCode_obis[match(tt$ID.new, OB_keys_and_orgs$ID.new)]
tt$InstitutionCode_obis <- OB_keys_and_orgs$InstitutionCode_obis[match(tt$ID.new, OB_keys_and_orgs$ID.new)]
tt$Resname_obis <- OB_keys_and_orgs$Resname_obis[match(tt$ID.new, OB_keys_and_orgs$ID.new)]
tt$ResourceID_obis <- OB_keys_and_orgs$ResourceID_obis[match(tt$ID.new, OB_keys_and_orgs$ID.new)]
head(tt[which(tt$SourceArchive=="GB_OB"),])

# MAREDAT keys and orgs
RecordIDs.for.which.we.want.mare.information <- tt[ which(tt$SourceArchive%in%c( "MA", "OB_MA", "GB_MA", "GB_OB_MA")), "ID.new"] # Define observations in the merged dataset of interest
MA_keys_and_orgs <- thekeys[ which(thekeys$ID.new%in%RecordIDs.for.which.we.want.mare.information & thekeys$SourceArchive%in%c( "MA")), c("ID.new", "OriginDatabase_mare", "CruiseOrStationID_mare", "SourceArchive")] # Get information from the non-merged data set for these observations
tt$OriginDatabase_mare <- MA_keys_and_orgs$OriginDatabase_mare[match(tt$ID.new, MA_keys_and_orgs$ID.new)]
tt$CruiseOrStationID_maredat <- MA_keys_and_orgs$CruiseOrStationID_mare[match(tt$ID.new, MA_keys_and_orgs$ID.new)]
tt$OriginDatabase_mare <- MA_keys_and_orgs$OriginDatabase_mare[match(tt$ID.new, MA_keys_and_orgs$ID.new)]
head(tt[which(tt$SourceArchive=="MA"),])

# Villar et al keys and orgs -> OK, since no merging occurred with respect to Villar et al. observation points
# Sal et al keys and orgs -> OK, since no merging occurred with respect to Sal et al. observation points

# Take a glance
head(tt)

# ADD INFORMATION ON ORIGINAL TAXON NAME TO RECORDS, BY SOURCES
# C) Fixed Parameters (using the function 'match')
# i.e., Add information to each record with respect to their original source GBIF, OBIS, MAREDAT, Sal et al. 2013, Villar et al. 2015)
########################################################################################

# Delete column
tt$taxon_name_original <- NULL

# Add original taxon names to gbif records
RecordIDs.for.which.we.want.gbif.information <- tt[ which(tt$SourceArchive%in%c("GB", "GB_OB", "GB_MA", "GB_OB_MA")), "ID.new"]
GB_taxon_names <- thekeys[ which(thekeys$ID.new%in%RecordIDs.for.which.we.want.gbif.information & thekeys$SourceArchive%in%c("GB")), c("ID.new", "scientificName_original")]
tt$taxonOriginal_gbif <- GB_taxon_names$scientificName_original[match(tt$ID.new, GB_taxon_names$ID.new)]

# Add original taxon names to obis records
RecordIDs.for.which.we.want.obis.information <- tt[ which(tt$SourceArchive%in%c("GB_OB",  "OB" ,  "OB_MA",  "GB_OB_MA")), "ID.new"]
OB_taxon_names <- thekeys[ which(thekeys$ID.new%in%RecordIDs.for.which.we.want.obis.information & thekeys$SourceArchive%in%c("OB")), c("ID.new", "scientificName_original") ]
tt$taxonOriginal_obis <- OB_taxon_names$scientificName_original[match(tt$ID.new, OB_taxon_names$ID.new)]

# Add original taxon names to maredat records
RecordIDs.for.which.we.want.mare.information <- tt[ which(tt$SourceArchive%in%c( "MA", "OB_MA", "GB_MA", "GB_OB_MA")), "ID.new"]
MA_taxon_names <- thekeys[ which(thekeys$ID.new%in%RecordIDs.for.which.we.want.mare.information & thekeys$SourceArchive%in%c("MA")), c("ID.new", "scientificName_original")]
tt$taxonOriginal_maredat <- MA_taxon_names$scientificName_original[match(tt$ID.new, MA_taxon_names$ID.new)]

# Add original taxon names to villar records
RecordIDs.for.which.we.want.villar.information <- tt[ which(tt$SourceArchive%in%c( "VI")), "ID.new"]
VI_taxon_names <- thekeys[ which(thekeys$ID.new%in%RecordIDs.for.which.we.want.villar.information & thekeys$SourceArchive%in%c("VI")), c("ID.new", "scientificName_original")]
tt$taxonOriginal_villar <- VI_taxon_names$scientificName_original[match(tt$ID.new, VI_taxon_names$ID.new)]

# Add original taxon names to sal records
RecordIDs.for.which.we.want.sal.information <- tt[ which(tt$SourceArchive%in%c( "SA")), "ID.new"]
SA_taxon_names <- thekeys[ which(thekeys$ID.new%in%RecordIDs.for.which.we.want.sal.information & thekeys$SourceArchive%in%c("SA")), c("ID.new", "scientificName_original")]
tt$taxonOriginal_sal <- SA_taxon_names$scientificName_original[match(tt$ID.new, SA_taxon_names$ID.new)]

# ADD ORIGINAL DEPTH ENTRIES TO RECORDS
original_depths_entries <- thekeys[ , c("ID.new", "depth_original")]
tt$depth_original <- original_depths_entries$depth_original[match(tt$ID.new, original_depths_entries$ID.new)]

### FINAL STEP: FLAGGING OF SEDIMENT OR SIMILAR DATA - by CHECKING METADATA ON THE CONTRIBUTING DATASETKEYS (GBIF; n = 1141 keys) OR RESOURCE IDs (OBIS; n = 75 resource names) 

### Load list with datasetkeys_gbif to be flagged
df.gbif <- read.csv("./Fossil_keys/GBIF_datasetkeys_fossil_2018_10_23.csv")
sum(df.gbif[1:12, "nobs"]) # 738'226
sum(df.gbif[1:12, "nobs"]) / sum(df.gbif[, "nobs"]) # The twelve largest dataset keys contain 93.42% of data
# Take a glance
df.gbif[1:20, 1:7]

### Load list with resourceids_obis to be flagged
df.obis <- read.csv("./Fossil_keys/OBIS_resource_IDs_fossil_2018_10_23.csv")
sum(df.obis[1:12, "nobs"]) # 781'961
sum(df.obis[1:12, "nobs"]) / sum(df.obis[, "nobs"]) # The twelve largest resource IDs contain 95.29% of data
# Take a glance
df.obis[1:20, 1:7]

### FLAGGING OF SEDIMENT BASED RECORDS
df.gbif$flag_suitable_DatasetKey_gbif <- as.character(df.gbif$flag_suitable_DatasetKey_gbif )
gbif.keys.to.be.excluded <- as.character(df.gbif[which(df.gbif$flag_suitable_DatasetKey_gbif == "no"), "DatasetKey_gbif"])
df.obis$flag_suitable_Resource_ID_obis <- as.character(df.obis$flag_suitable_Resource_ID_obis) 
obis.keys.to.be.excluded <- as.character(df.obis[which(df.obis$flag_suitable_Resource_ID_obis == "no"), "Resource_ID_obis"])

# GBIF
tt$basisPresumablySedimentary <- "NO"
dim(tt[which(tt$DatasetKey_gbif%in%gbif.keys.to.be.excluded), ]) # 29'575
tt[which(tt$DatasetKey_gbif%in%gbif.keys.to.be.excluded), "basisPresumablySedimentary"] <- "YES"

# OBIS
dim(tt[which(tt$ResourceID_obis%in%obis.keys.to.be.excluded), ]) # 18'575
tt[which(tt$ResourceID_obis%in%obis.keys.to.be.excluded), "basisPresumablySedimentary"] <- "YES"

# Per centage of flagged records
nrow(tt[which(tt$basisPresumablySedimentary == "YES"), ]) / nrow(tt) # 2.693%
dim(tt) # 1'360'765 x 48

### FINAL STEP: FLAGGING OF UNREALISTIC DAY OR YEAR OF COLLECTION

# Add column
tt$unrealisticDayOrYear <- "NO"

# Year
tt$year <- as.numeric(tt$year)
dim(tt[which(tt$year < 1800), ]) # 557 observation points -> all in picophytoplankton dataset
unique(tt[which(tt$year < 1800), "year"]) # 6, 10, 11
dim(tt[which(tt$year == 6), ]) # 380
dim(tt[which(tt$year == 10), ]) # 67
dim(tt[which(tt$year == 11), ]) # 110
tt[which(tt$year == 6 | tt$year == 10 | tt$year == 11), "unrealisticDayOrYear"] <- "YES" # Flag

# Day
tt$day <- as.numeric(tt$day)
dim(tt[which(tt$day < 0 | tt$day > 31), ]) # 25'882 observation points -> all in picophytoplankton dataset
unique(tt[which(tt$day < 0 | tt$day > 31), "day"]) # -9, -1
dim(tt[which(tt$day == -9), ]) # 25'764 observation points -> in picophytoplankton dataset	
dim(tt[which(tt$day == -1), ]) # 118 observation points -> in picophytoplankton dataset	
tt[which(tt$day == -9 | tt$day == -1), "unrealisticDayOrYear"] <- "YES" # Flag
	
# Month
unique(tt$month) # fine

# SAVE DATABASE: Select or adjust column names, field entries, spelling of field entries, and save harmonized database as .csv file

# 1. Adjust column field entries: spelling
tt$occurrenceStatus <- "PRESENT"
tt[ which(tt$BasisOfRecord == "flowcytometry"), "BasisOfRecord" ] <- "FLOWCYTOMETRY"
unique(tt$SourceArchive)
tt[ which(tt$SourceArchive == "GB"), "SourceArchive" ] <- "GBIF"
tt[ which(tt$SourceArchive == "GB_OB"), "SourceArchive" ] <- "GBIF_OBIS"
tt[ which(tt$SourceArchive == "MA"), "SourceArchive" ] <- "MAREDAT"
tt[ which(tt$SourceArchive == "OB"), "SourceArchive" ] <- "OBIS"
tt[ which(tt$SourceArchive == "SA"), "SourceArchive" ] <- "SAL"
tt[ which(tt$SourceArchive == "OB_MA"), "SourceArchive" ] <- "OBIS_MAREDAT"
tt[ which(tt$SourceArchive == "VI"), "SourceArchive" ] <- "VILLAR"
tt[ which(tt$SourceArchive == "GB_OB_MA"), "SourceArchive" ] <- "GBIF_OBIS_MAREDAT"
tt[ which(tt$SourceArchive == "GB_MA"), "SourceArchive" ] <- "GBIF_MAREDAT"
unique(tt$SourceArchive)

# 2a. Check CoordinateUncertaintyInMeters: this column is discarded below
dim(tt[ !is.na(tt$CoordinateUncertaintyInMeters), ]) # Only 4'025 entries with values
unique(as.character(tt[ !is.na(tt$CoordinateUncertaintyInMeters), "CoordinateUncertaintyInMeters"]))
dim(tt[ which(tt$CoordinateUncertaintyInMeters ==1000), ]) # Only 1 value with uncertainty values equal to 1000m

# 2b. Check DepthAccuracy: column is retained
dim(tt[ !is.na(tt$DepthAccuracy), ]) # At least 129'541 entries with values
unique(tt[ !is.na(tt$DepthAccuracy), "DepthAccuracy"])

# 3. Mixed layer depth: retained as a classifier - i.e. do records fall inside the monthly climatological mixed-layer (de Boyer Montegut, 2004; temperature criterion)?
tt$recordWithinMLD_clim <- "NA"
tt[ which(tt$depth<=tt$MLD1), "recordWithinMLD_clim" ] <- "YES"
tt[ which(tt$depth>tt$MLD1), "recordWithinMLD_clim" ] <- "NO"

# 4. Retain information on insitu measured mixed-layer depth with a maximum of two digits after the comma
tt$insituMLD <- round(tt$insituMLD, 2)

# 5. Homogenize unknown basis of record and NA basis of record to NA
tt$BasisOfRecord <- as.character(tt$BasisOfRecord)
tt[ is.na(tt$BasisOfRecord), "BasisOfRecord"] <- "NA"
tt[ which(tt$BasisOfRecord == "UNKNOWN"), "BasisOfRecord" ] <- "NA"

# 6. Change unknown taxonomic rank  to NA
tt[ which(tt$TaxonRank == "UNKNOWN"), "TaxonRank" ] <- "NA"

# 7. Change column names
colnames(tt) [which (colnames(tt)%in%c("taxon"))] <- "scientificName"
colnames(tt) [which (colnames(tt)%in%c("longitude"))] <- "decimalLongitude"
colnames(tt) [which (colnames(tt)%in%c("latitude"))] <- "decimalLatitude"
colnames(tt) [which (colnames(tt)%in%c("TaxonRank"))] <- "taxonRank"
colnames(tt) [which (colnames(tt)%in%c("BasisOfRecord"))] <- "basisOfRecord"
colnames(tt) [which (colnames(tt)%in%c("DepthAccuracy"))] <- "depthAccuracy"
colnames(tt) [which (colnames(tt)%in%c("SourceArchive"))] <- "sourceArchive"
colnames(tt) [which (colnames(tt)%in%c("PublishingOrgKey_gbif"))] <- "publishingOrgKey_gbif"
colnames(tt) [which (colnames(tt)%in%c("InstitutionCode_gbif"))] <- "institutionCode_gbif"
colnames(tt) [which (colnames(tt)%in%c("InstitutionCode_gbif"))] <- "institutionCode_gbif"
colnames(tt) [which (colnames(tt)%in%c("DatasetKey_gbif"))] <- "datasetKey_gbif"
colnames(tt) [which (colnames(tt)%in%c("InstitutionCode_obis"))] <- "institutionCode_obis"
colnames(tt) [which (colnames(tt)%in%c("InstitutionCode_obis"))] <- "institutionCode_obis"
colnames(tt) [which (colnames(tt)%in%c("CollectionCode_obis"))] <- "collectionCode_obis"
colnames(tt) [which (colnames(tt)%in%c("ResourceID_obis"))] <- "resourceID_obis"
colnames(tt) [which (colnames(tt)%in%c("Resname_obis"))] <- "resname_obis"
colnames(tt) [which (colnames(tt)%in%c("OriginDatabase_mare"))] <- "originDatabase_maredat"
colnames(tt) [which (colnames(tt)%in%c("CruiseOrStationID_mare"))] <- "cruiseOrStationID_maredat"
colnames(tt) [which (colnames(tt)%in%c("TaraStation_villar"))] <- "taraStation_villar"
colnames(tt) [which (colnames(tt)%in%c("Cruise_sal"))] <- "cruise_sal"
colnames(tt) [which (colnames(tt)%in%c("SampleID_sal"))] <- "sampleID_sal"
colnames(tt) [which (colnames(tt)%in%c("insituMLD"))] <- "MLD_insitu"
colnames(tt) [which (colnames(tt)%in%c("YearOfDataAccess"))] <- "yearOfDataAccess"
colnames(tt) [which (colnames(tt)%in%c("IndividualCount"))] <- "individualCount"
colnames(tt) [which (colnames(tt)%in%c("VolumeBasisOfIndividualCount"))] <- "volumeBasisOfIndividualCount"
colnames(tt) [which (colnames(tt)%in%c("Cells_per_l"))] <- "cellsPerLitre"
colnames(tt) [which (colnames(tt)%in%c("Colonial_form_cells_per_l"))] <- "colonialFormCellsPerLitre"
colnames(tt) [which (colnames(tt)%in%c("Total_colonial_or_single_cells_or_trichomes_l"))] <- "totalColonialorSingleCells_or_trichomes_l"
colnames(tt) [which (colnames(tt)%in%c("depth_original"))] <- "depthOriginal"

# 8. Adjust column field entries for records with IndividualCount == 0, or CellsPerLitre == 0
tt[which(tt$IndividualCount == 0), ] # zero rows, there are none
tt[which(tt$CellsPerLitre == 0), ] # zero rows, there are none

# 9. Rename ID to ID_orig_taxon, rename ID.new.final to ID
tt$ID_orig_taxon <- tt$ID
tt$ID <- tt$ID.new.final

# Check: Dimension absences, shall be zero in all cases
dim(tt[which(tt$colonial_form_cells_l == 0) , ]) # 0
dim(tt[which(tt$cells_l == 0) , ]) # 0
dim(tt[which(tt$cells_l == 0 & tt$colonial_form_cells_l == 0) , ]) # 0
dim(tt[which(tt$colonial_form_cells_l > 0),]) # 0

# 10. Select and order columns if interest
tt.write <- tt[ , c("ID", "scientificName", "group", "decimalLongitude", "decimalLatitude", "year", "month", "day", "depth", "depthOriginal","unrealisticDayOrYear",
"taxonRank", "basisOfRecord", "basisPresumablySedimentary", "occurrenceStatus", "depthAccuracy", 
"sourceArchive", "publishingOrgKey_gbif", "institutionCode_gbif", "datasetKey_gbif", "institutionCode_obis", "collectionCode_obis", "resourceID_obis", "resname_obis", 
"originDatabase_maredat", "cruiseOrStationID_maredat", "taraStation_villar", "cruise_sal", "sampleID_sal", "MLD_insitu", "recordWithinMLD_clim", 
"yearOfDataAccess", "individualCount", "volumeBasisOfIndividualCount", "cellsPerLitre", 
"colonialFormCellsPerLitre", "totalColonialorSingleCells_or_trichomes_l", "taxonOriginal_gbif",  "taxonOriginal_obis",  "taxonOriginal_maredat", "taxonOriginal_villar", "taxonOriginal_sal")]

# Write data frame
dim(tt.write) # 1'360'765 x 42
fln <- "./Data_final_product/Phytoplankton_harmonized_database.csv"
write.csv(tt.write, file = fln, row.names = F)





## PS: Note that the species synonym table, and the tables containing the dataset keys, are provided along with this code in .csv format