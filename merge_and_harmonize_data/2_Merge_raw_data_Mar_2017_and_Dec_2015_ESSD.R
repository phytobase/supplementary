### MERGE MASKED RAW DATA OF DATA DOWNLOAD VERSION EARLY 2017 WITH DATA DOWNLOAD VERSION LATE 2015 ###

# Date: 2018-10-02
#
# Author: Damiano Righetti, rdamiano@ethz.ch
# Environmental Physics Group, Institute of Biogeochemistry
# and Pollutant Dynamics, ETH Zurich, CH

# Description: Merge raw data points across data sources
# Input: 2015 database (OBIS_2015, GBIF_2015, MAREDAT_2013, Villar_et_al_2015)
# Input: 2017 database (OBIS_2017, GBIF_2017, MAREDAT_2013, Villar_et_al_2015, Sal_et_al_2013)
# Output: merged frame of desired format: 
# x, y, year, month, day, taxon, group, rank, source, basis of record, depth, mld (from masking step GBIF and OBIS, or in situ VILLAR), and information on original dataset key and the collecting organisations

### =========================================================================
### Preparatory steps
### =========================================================================

rm(list = ls())
library(doBy)

### =========================================================================
### Inputs and Outputs
### =========================================================================

# Working directory
setwd("~/Desktop/PHYTO_DATA_ESSD") # Define name of the working directory

### =========================================================================
### Preparatory
### =========================================================================

# Define columns of interest
cols <- c(
"ID", "taxon", "group", "longitude", "latitude", "year", "month", "day", "depth", "taxonRank", "BasisOfRecord", "OccurrenceStatus", "SourceArchive", # essentials
"coordinateUncertaintyInMeters", "depthAccuracy", # spatial uncertainties
"Cells_per_l",  "Colonial_form_cells_per_l", "Total_colonial_or_single_cells_or_trichomes_l", "IndividualCount", "VolumeBasisOfIndividualCount", # abundance  
"PublishingOrgKey_gbif", "InstitutionCode_gbif", "DatasetKey_gbif", # organisation ID and dataset/cruise ID gbif
"InstitutionCode_obis", "CollectionCode_obis", "Resname_obis", "ResourceID_obis", # organisation ID and dataset/cruise ID obis
"OriginDatabase_mare", "CruiseOrStationID_mare",  # organisation ID and dataset/cruise ID mare
"TaraStation_villar", # station ID Villar et al (part of the TARA oceans expedition)
"Cruise_sal", "SampleID_sal" , # cruise/sample ID Sal et al (2013 microphytoplankton database)
"insituMLD", "MLD1" # physics
 )

### =========================================================================
### Merge databases 2015 and 2017 (removing duplicates, melding information)
### =========================================================================

# 1. get data
dat_2015 <- get(load('~/Desktop/PHYTO_DATA_ESSD/Data_intermediate_steps/Species_obs_raw_2015.12.17.RData'))
dat_2017 <- get(load('~/Desktop/PHYTO_DATA_ESSD/Data_intermediate_steps/Species_obs_raw_2017.03.06.RData'))

# 2. add missing columns dat_2015 (as NA vectors)
for (k in 1:length(cols)){attrib <- cols[k]; if(attrib%in%colnames(dat_2015)==F){paste0("Warning: No ",  attrib , " ", dat_2015[ 1, "family"]," ..."); dat_2015$new.col <- NA; names(dat_2015)[names(dat_2015)=="new.col"]<-attrib}}
dat_2015 <- dat_2015[ , cols]
dat_2015$version <- "2015"

# 3. add missing columns dat_2017 (as NA vectors)
for (k in 1:length(cols)){attrib <- cols[k]; if(attrib%in%colnames(dat_2017)==F){paste0("Warning: No ",  attrib , " ", dat_2017[ 1, "family"]," ..."); dat_2017$new.col <- NA; names(dat_2017)[names(dat_2017)=="new.col"]<-attrib}}
dat_2017 <- dat_2017[ , cols]
dat_2017$version <- "2017"

# 4. merge the two datasets
base <- rbind(dat_2015, dat_2017)
dim(base) # 2'220'002 x 35

# 5. preparatory: for merging information all relevant data-fields need to be of class "character"
base$IndividualCount <- as.character(base$IndividualCount)
base$Cells_per_l <- as.character(base$Cells_per_l)
base$Colonial_form_cells_per_l <- as.character(base$Colonial_form_cells_per_l)
base$Total_colonial_or_single_cells_or_trichomes_l <- as.character(base$Total_colonial_or_single_cells_or_trichomes_l)
base[which(base$Total_colonial_or_single_cells_or_trichomes_l == "NA"), "Total_colonial_or_single_cells_or_trichomes_l"] <- NA

### MELD INFORMATION, WHEN REMOVING DUPLICATES
##########################################################

## PREPARATION: Prepare a column indicating the original version ("version) or dataset ("set") in the merged product ("base"), removing duplicated observation records by aggregating data to IDs (collapsing data in which IDs are duplicated, in case there are duplicates)

# Year of data assembly and source
vec.version <- aggregate(version~ID, data=base, paste, collapse="_") # nrow(vec.version) = 1'594'649
vec.sourcearchive <- aggregate(SourceArchive~ID, data=base, paste, collapse="_") # nrow(vec.sourcearchive) = 1'594'649

# Abundance
vec.individcounts <- aggregate(IndividualCount~ID, data=base, paste, collapse="_", na.action = na.pass)
vec.VolumeBasisOfIndividualCount <- aggregate(VolumeBasisOfIndividualCount~ID, data=base, paste, collapse="_", na.action = na.pass)
vec.cellsperlitre <- aggregate(Cells_per_l~ID, data=base, paste, collapse="_", na.action = na.pass)
vec.colonialcells <- aggregate(Colonial_form_cells_per_l~ID, data=base, paste, collapse="_", na.action = na.pass)
vec.colonialorsinglecells <- aggregate(Total_colonial_or_single_cells_or_trichomes_l~ID, data=base, paste, collapse="_", na.action = na.pass)

### YEAR AND SOURCE
####################

# IMPLEMENTATION 1: Remove duplicates in "base" regarding the species-point IDs (x,y,year,month,day,taxon,depth)
base.new <- base[!duplicated(base$ID), ] 
dim(base.new) # 1'594'649 x 35

# IMPLEMENTATION 2: Add the "version" data-field, collapse regarding observation IDs, add "version" information (vec.version, see above), using the matching key == ID
tt <- merge( base.new[,-which(names(base.new)=="version")], vec.version, by ="ID")
unique(tt$version)
head(tt)

# IMPLEMENTATION 3: Add the "set" data-field, collapse data regarding observation IDs, add "set" information
tt <- merge( tt[,-which(names(tt)=="SourceArchive")], vec.sourcearchive, by ="ID")
# adjustments: 
tt[which(tt$SourceArchive == "GB_GB"), "SourceArchive"] <- "GB"
tt[which(tt$SourceArchive == "GB_OB_OB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==  "GB_OB_GB_OB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==  "OB_OB"), "SourceArchive"] <- "OB"
tt[which(tt$SourceArchive ==  "MA_MA"), "SourceArchive"] <- "MA"
tt[which(tt$SourceArchive ==  "GB_OB_GB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==  "GB_GB"), "SourceArchive"] <- "GB"
tt[which(tt$SourceArchive ==  "OB_GB_OB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==  "OB_GB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==  "OB_GB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==  "GB_GB_OB"), "SourceArchive"] <- "GB_OB"
tt[which(tt$SourceArchive ==  "OB_MA_MA"), "SourceArchive"] <- "OB_MA"
tt[which(tt$SourceArchive ==  "OB_MA_GB_MA"), "SourceArchive"] <- "GB_OB_MA"
tt[which(tt$SourceArchive ==  "GB_MA_MA"), "SourceArchive"] <- "GB_MA"
tt[which(tt$SourceArchive ==  "GB_MA_GB_MA"), "SourceArchive"] <- "GB_MA"
tt[which(tt$SourceArchive ==  "GB_OB_MA_MA"), "SourceArchive"] <- "GB_OB_MA"
tt[which(tt$SourceArchive ==   "GB_OB_MA_GB_MA"), "SourceArchive"] <- "GB_OB_MA"
tt[which(tt$SourceArchive ==   "OB_MA_OB_MA"), "SourceArchive"] <- "OB_MA"
tt[which(tt$SourceArchive ==   "VI_VI"), "SourceArchive"] <- "VI"
tt[which(tt$SourceArchive ==   "GB_OB_MA_GB_OB_MA"), "SourceArchive"] <- "GB_OB_MA"
tt[which(tt$SourceArchive ==   "MA_OB_MA"), "SourceArchive"] <- "OB_MA"
unique(tt$SourceArchive)
head(tt)

# ABUNDANCE
##############

# IMPLEMENTATION 4A: Add the "IndividualCounts" data-field, collapse data regarding observation IDs, add "vec.individcounts" information, using the matching key == ID
tt2 <- merge( tt[,-which(names(tt)=="IndividualCount")], vec.individcounts, by ="ID")
unique(tt2$IndividualCount)
# adjustments:
vv <- tt2$IndividualCount
vv[which(vv == "NA_NA")] <- "NA"
vvnew <- cbind(sapply(vv, FUN = function(x){x1 <- gsub ("NA_","",x); return(x1)})) # takes 10 sec
length(vvnew)
tt2$IndividualCount <- vvnew
unique(tt2$IndividualCount)
head(tt2)

# IMPLEMENTATION 4B: Add the "VolumeBasisOfIndividualCount" data-field, collapse data regarding observation IDs, add "vec.VolumeBasisOfIndividualCount" information, using the matching key == ID
tt3 <- merge( tt2[,-which(names(tt2)=="VolumeBasisOfIndividualCount")], vec.VolumeBasisOfIndividualCount, by ="ID")
unique(tt3$VolumeBasisOfIndividualCount)
# adjustments:
tt3[which(tt3$VolumeBasisOfIndividualCount == "NA_NA"), "VolumeBasisOfIndividualCount"] <- "NA"
tt3[which(tt3$VolumeBasisOfIndividualCount == "NA_1_litre"), "VolumeBasisOfIndividualCount"] <- "1_litre"
unique(tt3$VolumeBasisOfIndividualCount)
head(tt3)

# IMPLEMENTATION 4C: Add the "Cells_per_l" data-field, collapse data regarding observation IDs, add "vec.cellsperlitre" information, using the matching key == ID
tt4 <- merge( tt3[,-which(names(tt3)=="Cells_per_l")], vec.cellsperlitre, by ="ID")
unique(tt4$Cells_per_l)
# adjustments:
vv <-tt4$Cells_per_l
vv[which(vv == "NA_NA")] <- "NA"
vvnew <- cbind(sapply(vv, FUN = function(x){x1 <- gsub ("NA_","",x); return(x1)})) # takes 10 sec
vvnew <- data.frame(do.call("rbind", strsplit(as.character (vvnew),"_" ,fixed =T)  ))
unique(vvnew)
tt4$Cells_per_l <- vvnew[ , 1]
unique(tt4$Cells_per_l)
head(tt4)

# IMPLEMENTATION 4D: Add the "Colonial_form_cells_per_l" data-field, collapse data regarding observation IDs, add "vec.colonialcells" information, using the matching key == ID
tt5 <- merge( tt4[,-which(names(tt4)=="Colonial_form_cells_per_l")], vec.colonialcells, by ="ID")
unique(tt5$Colonial_form_cells_per_l)
# adjustments:
vv <-tt5$Colonial_form_cells_per_l
vv[which(vv == "NA_NA")] <- "NA"
vvnew <- cbind(sapply(vv, FUN = function(x){x1 <- gsub ("NA_","",x); return(x1)})) # takes 10 sec
vvnew <- data.frame(do.call("rbind", strsplit(as.character (vvnew),"_" ,fixed =T)  ))
unique(vvnew)
tt5$Colonial_form_cells_per_l <- vvnew[ , 1]
unique(tt5$Colonial_form_cells_per_l)
head(tt5)

# IMPLEMENTATION 4E: Add the "Total_colonial_or_single_cells_or_trichomes_l" data-field, collapse data regarding observation IDs, add "vec.colonialorsinglecells" information, using the matching key == ID
tt6 <- merge( tt5[,-which(names(tt5)=="Total_colonial_or_single_cells_or_trichomes_l")], vec.colonialorsinglecells, by ="ID")
unique(tt6$Total_colonial_or_single_cells_or_trichomes_l)
# adjustments:
vv <-tt6$Total_colonial_or_single_cells_or_trichomes_l
vv[which(vv == "NA_NA")] <- "NA"
vvnew <- cbind(sapply(vv, FUN = function(x){x1 <- gsub ("NA_","",x); return(x1)})) # takes 10 sec
vvnew <- data.frame(do.call("rbind", strsplit(as.character (vvnew),"_" ,fixed =T)  ))
unique(vvnew)
tt6$Total_colonial_or_single_cells_or_trichomes_l <- vvnew[ , 1]
unique(tt6$Total_colonial_or_single_cells_or_trichomes_l)
head(tt6)

# TAXON RANK
##############

# IMPLEMENTATION 5: change taxon rank where needed
tt <- tt6
tt$taxonRank <- as.character(tt$taxonRank)
unique(tt$taxonRank)
unique(tt[which(tt$taxonRank == "FORM" | tt$taxonRank == "FORMA"), "taxon"])
# We thus adjust any field entries for form to forma
tt[which(tt$taxonRank == "FORM"), "taxonRank"] <- "FORMA"# Change FORM to FORMA
tt[is.na(tt$taxonRank), "taxonRank"] <- "UNKNOWN" # Change NA to UNKNOWN
# We adjust rank to current rank
tt[which(tt$taxonRank == "FORMA"), "taxonRank"] <- "SPECIES"
unique(tt[which(tt$taxonRank == "GENUS"), "taxon"]) # FINE! These are at level genus
unique(tt[which(tt$taxonRank == "VARIETY"), "taxon"]) # These are now at level species
tt[which(tt$taxonRank == "VARIETY"), "taxonRank"] <- "SPECIES"
unique(tt[which(tt$taxonRank == "SUBSPECIES"), "taxon"]) # These are now at level species
tt[which(tt$taxonRank == "SUBSPECIES"), "taxonRank"] <- "SPECIES"
unique(tt[which(tt$taxonRank == "UNKNOWN"), "taxon"]) # Ok, these are at level unknown

# COLUMNS
###########

# IMPLEMENTATION 6: change column names
colnames(tt)[which(colnames(tt)%in%c("taxonRank"))] <- "TaxonRank"
colnames(tt)[which(colnames(tt)%in%c("version"))] <- "YearOfDataAccess"
colnames(tt)[which(colnames(tt)%in%c("individualCount"))] <- "IndividualCount"
colnames(tt)[which(colnames(tt)%in%c("coordinateUncertaintyInMeters"))] <- "CoordinateUncertaintyInMeters"
colnames(tt)[which(colnames(tt)%in%c("depthAccuracy"))] <- "DepthAccuracy"
colnames(tt)[which(colnames(tt)%in%c("set"))] <- "SourceArchive"
head(tt)
dim(tt) # 1'594'649 x 35

# ADD INFORMATION ON DATASETKEYS AND ORGS (To each observation record, with respect to their original source GBIF, OBIS, MAREDAT, Sal et al 2013, Villar et al. 2015)
#########################################################################################################################################

# Gbif keys and orgs
unique(tt$SourceArchive)
RecordIDs.for.which.we.want.gbif.information <- tt[ which(tt$SourceArchive%in%c("GB", "GB_OB", "GB_MA", "GB_OB_MA")), "ID"] # Define observations in the merged and collapsed dataset of interest to add information
GB_keys_and_orgs <- base[ which(base$ID%in%RecordIDs.for.which.we.want.gbif.information & base$SourceArchive%in%c("GB", "GB_OB", "GB_MA", "GB_OB_MA")), c("ID", "DatasetKey_gbif", "PublishingOrgKey_gbif", "InstitutionCode_gbif", "SourceArchive")] # Get information from the non-merged data set for these observations (constrain to obis; as same IDs can be found in GB and OBIS / multiple sources)
tt$DatasetKey_gbif <- GB_keys_and_orgs$DatasetKey_gbif[match(tt$ID, GB_keys_and_orgs$ID)] # Add this information to each record
tt$PublishingOrgKey_gbif <- GB_keys_and_orgs$PublishingOrgKey_gbif[match(tt$ID, GB_keys_and_orgs$ID)] # Add this information to each record
tt$InstitutionCode_gbif <- GB_keys_and_orgs$InstitutionCode_gbif[match(tt$ID, GB_keys_and_orgs$ID)] # Add this information to each record
#
# Obis keys and orgs
RecordIDs.for.which.we.want.obis.information <- tt[ which(tt$SourceArchive%in%c("GB_OB",  "OB" ,  "OB_MA",  "GB_OB_MA")), "ID"] # Define observations in the merged dataset of interest to add information
OB_keys_and_orgs <- base[ which(base$ID%in%RecordIDs.for.which.we.want.obis.information & base$SourceArchive%in%c("GB_OB",  "OB" ,  "OB_MA",  "GB_OB_MA")), c("ID", "CollectionCode_obis", "InstitutionCode_obis",  "Resname_obis", "ResourceID_obis", "SourceArchive")] # Get information from the non-merged data set for these observations (constrain to obis; as same IDs can be found in GB and OBIS / multiple sources)
tt$CollectionCode_obis <- OB_keys_and_orgs$CollectionCode_obis[match(tt$ID, OB_keys_and_orgs$ID)]
tt$InstitutionCode_obis <- OB_keys_and_orgs$InstitutionCode_obis[match(tt$ID, OB_keys_and_orgs$ID)]
tt$Resname_obis <- OB_keys_and_orgs$Resname_obis[match(tt$ID, OB_keys_and_orgs$ID)]
tt$ResourceID_obis <- OB_keys_and_orgs$ResourceID_obis[match(tt$ID, OB_keys_and_orgs$ID)]
head(tt)
#
# Maredat keys and orgs
RecordIDs.for.which.we.want.mare.information <- tt[ which(tt$SourceArchive%in%c( "MA", "OB_MA", "GB_MA", "GB_OB_MA")), "ID"] # Define observations in the merged dataset of interest to add information
MA_keys_and_orgs <- base[ which(base$ID%in%RecordIDs.for.which.we.want.mare.information & base$SourceArchive%in%c( "MA", "OB_MA", "GB_MA", "GB_OB_MA")), c("ID", "OriginDatabase_mare", "CruiseOrStationID_mare", "SourceArchive")] # Get information from the non-merged data set for these observations (constrain to obis; as same IDs can be found in GB and OBIS / multiple sources)
tt$OriginDatabase_mare <- MA_keys_and_orgs$OriginDatabase_mare[match(tt$ID, MA_keys_and_orgs$ID)]
tt$CruiseOrStationID_mare <- MA_keys_and_orgs$CruiseOrStationID_mare[match(tt$ID, MA_keys_and_orgs$ID)]
tt$OriginDatabase_mare <- MA_keys_and_orgs$OriginDatabase_mare[match(tt$ID, MA_keys_and_orgs$ID)]
#
# Villar et al keys and orgs
RecordIDs.for.which.we.want.villar.information <- tt[ which(tt$SourceArchive%in%c( "VI" )), "ID"] # Define observations in the merged dataset of interest to add information
VI_keys_and_orgs <- base[ which(base$ID%in%RecordIDs.for.which.we.want.villar.information & base$SourceArchive == "VI"), c("ID", "TaraStation_villar", "SourceArchive")] # Get information from the non-merged data set for these observations (constrain to obis; as same IDs can be found in GB and OBIS / multiple sources)
tt$TaraStation_villar <- VI_keys_and_orgs$TaraStation_villar[match(tt$ID, VI_keys_and_orgs $ID)]
#
# Sal et al keys and orgs
RecordIDs.for.which.we.want.sal.information <- tt[which(tt$SourceArchive%in%c("SA")), "ID"]
SA_keys_and_orgs <- base[ which(base$ID%in%RecordIDs.for.which.we.want.sal.information & base$SourceArchive == "SA"), c("ID", "Cruise_sal", "SampleID_sal" ,"SourceArchive")] # Get information from the non-merged data set for these observations (constrain to obis; as same IDs can be found in GB and OBIS / multiple sources)
tt$Cruise_sal <- SA_keys_and_orgs$Cruise_sal[match(tt$ID, SA_keys_and_orgs$ID)]
tt$SampleID_sal <- SA_keys_and_orgs$SampleID_sal[match(tt$ID, SA_keys_and_orgs$ID)]
#
# Take a glance
head(tt)
#
# Save data archives 2015 with 2017 merged (non-homogenized)
fln <-paste0("~/Desktop/PHYTO_DATA_ESSD/Data_intermediate_steps/Species_obs_raw_united_2017_with_2015.RData")
save(tt, file=fln)
