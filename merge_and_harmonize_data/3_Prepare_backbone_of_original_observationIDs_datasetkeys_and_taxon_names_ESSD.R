### CREATE BACKBONE OF DATASET KEYS OR ORGANISATIONS ASSOCIATED WITH RAW OBSERVATION POINTS (FROM GBIF, OBIS, MAREDAT, VILLAR, SAL)

# Date: 2018-10-21
#
# Author: Damiano Righetti, rdamiano@ethz.ch
# Environmental Physics Group, Institute of Biogeochemistry
# and Pollutant Dynamics, ETH Zurich, CH

# Description: This "backbone" serves as a basis to retain information on the source datasets / datasetkeys for OBIS, GBIF, Villar et al, and MAREDAT with phytoplantkon observation points in the final merged and harmonized database
# Thus, even if records are later merged and/or taxa harmonized, information on each record with respect to the source dataset can be conserved
# Step 1 - Merge the raw data sources into a single data frame, without removing duplicates and without harmonizing taxon names
# Step 2 - Generate an ID for the original taxon names / records (the ID is unique to each record, using time, location and taxonomy)
# Step 3 - Add second column with harmonized taxon names, without merging / removing duplicates
# Step 4 - Add second column with ID.new (the ID that is unique to each record, using time, location and taxonomy - but now based on the harmonized taxon names)

# Clean home
rm(list = ls())
setwd("~/Desktop/PHYTO_DATA_ESSD/") # Define name of the working directory

# Packages
library(doBy)
library(sfsmisc)
library(doParallel)
library(foreach)

# Define columns of interest
cols <- c("ID", "taxon", "longitude", "latitude", "year", "month", "day", "depth", "depth_original", "group", 
"PublishingOrgKey_gbif", "InstitutionCode_gbif", "DatasetKey_gbif", 
"InstitutionCode_obis", "CollectionCode_obis", "Resname_obis", "ResourceID_obis",
"OriginDatabase_mare", "CruiseOrStationID_mare",
"TaraStation_villar", "Cruise_sal", "SampleID_sal", "MLD1", "insituMLD","SourceArchive" )

# Load non-melted and non-harmonized data (merged versions)
part15 <- get(load("./Data_intermediate_steps/intermediate_save_base_2015_with_ID.RData"))
colnames(part15)[which(colnames(part15)%in%c("set"))] <- "SourceArchive"
head(part15)

part17 <- get(load("./Data_intermediate_steps/intermediate_save_base_2017_with_ID.RData"))
colnames(part17)[which(colnames(part17)%in%c("set"))] <- "SourceArchive"
head(part17)

# Select columns of interest
p15 <- part15[ , cols]
p17 <- part17[ , cols]

# Merge 
pp <- rbind(p15, p17)
dim(pp) # 2'599'643 x 25
pp <- pp[!duplicated(pp), ]
dim(pp) # 2'497'022 x 25

# For interest
sum(duplicated(pp$ID)) # 902'373

# ------------------------------ Step 1: Select non NA-depth taxa -----------------------------------

### ---------- Step one - preparatory with regard to harmonisation: exclude species that have no single depth-referenced record (as these species were not considered for the species synonym table, and hence cannot be harmonised)
pp$taxon <- as.character(pp$taxon)
dat.split <- split(pp, f = pp$taxon)
is.na.depth <- vector()
for(i in 1:length(dat.split)){
data.species <- dat.split[[i]]
if(   is.na(mean(data.species$depth, na.rm=T)) == T   ){   is.na.depth[i] <- "exclude"   } else {   is.na.depth[i] <- "fine"   }
print(paste(i))
}
length(is.na.depth) # 4'741 original species names
length(is.na.depth[which(is.na.depth=="exclude")]) # 1'441 species excluded
length(is.na.depth[which(is.na.depth=="fine")]) # 3'300 species kept
pp.new <- dat.split[which(is.na.depth=="fine")] # Exclude the non-depth referenced / douptful species
ppp <- do.call(rbind, pp.new) # Takes < 1 minute
dim(ppp) # 2'490'688 x 25

### ---------- Step two - prepare the synonym table for step two: and harmonize the data
tab <- read.csv("./Synonym_table_total/Synonym_table_2018_09_28.csv") # Get the synonym table (species synonym table) containing all 2015 and 2017 dataset species (genera)
tab$scientificName_original <- as.character(tab$scientificName_original)
tab$scientificName_harmonized <- as.character(tab$scientificName_harmonized)

# Rename data frame
tot.raw.uni1 <- ppp
tot.raw.uni1$scientificName_original <- tot.raw.uni1$taxon
tot.raw.uni1$scientificName_original <- as.character(tot.raw.uni1$scientificName_original)
tot.raw.uni1$taxon <- "NA" # Set taxon name to NA, serves as placeholder for the new (harmonized) taxon name

# Add Id for re-ordering the data
tot.raw.uni1$Id <- 1:nrow(tot.raw.uni1)

# Are all taxa of the dataset contained in the synonym table?
length (unique(tot.raw.uni1$scientificName_original)) # 3300
sum(  unique(tot.raw.uni1$scientificName_original)%in%tab$scientificName_original) # OK, all contained
unique(tot.raw.uni1$scientificName_original)[which(unique(tot.raw.uni1$scientificName_original)%in%tab$scientificName_original == F)] # 0, QED
 
# Harmonization - loop across taxa to assign new names to original names - takes some 5 minutes
lisi.taxa <- list()
for (i in 1:length(unique (tot.raw.uni1$scientificName_original))){
dat.tax <- tot.raw.uni1 [  which  (  tot.raw.uni1$scientificName_original == unique (tot.raw.uni1$scientificName_original)[i]  )  ,  ]
dat.tax$taxon <- as.character(tab[ which( tab$scientificName_original == dat.tax$scientificName_original[1]), "scientificName_harmonized"])
dat.tax$flag_suitable_species <- as.character(tab[ which( tab$scientificName_original == dat.tax$scientificName_original[1]), "isSelectedForPhytoplanktonDatabase"])
lisi.taxa [[i]] <- dat.tax
print(paste(i))
}

# Merge data
tt.uni <- do.call(rbind, lisi.taxa) # Takes some 1'
tt.uni <- tt.uni[order(tt.uni$Id), ]
tt.uni$Id <- NULL
rownames(tt.uni) <- 1:nrow(tt.uni)
dim(tt.uni) # 2'490'688 x 27

######################### Intermediate save one ############################
fln <- "./Data_intermediate_steps/total_keys_mid_save_one.RData"
save(tt.uni, file = fln)
tt.united <- get(load("./Data_intermediate_steps/total_keys_mid_save_one.RData"))
########################################################################

### ------------------------ Step three Create ID.new for novel taxa --------------------------------------
# Rename ID
tt.united$ID.original <- tt.united$ID # ***KEY OPERATION***
# Adjust class
tt.united$longitude <- as.character(tt.united$longitude)
tt.united$latitude <- as.character(tt.united$latitude)
tt.united$year <- as.character(tt.united$year)
tt.united$month <- as.character(tt.united$month)
tt.united$day <- as.character(tt.united$day)
tt.united$depth <- as.character(tt.united$depth)
head(tt.united) 
dim(tt.united) # 2'490'688 x 28

# Generate observation point ID with regard to: taxon, group, x, y, year, month, day, depth - takes some 240 minutes, run on seven cores in Mac terminal
n.cores <- 7
cl = makeCluster(n.cores, outfile="") #outfile="" returns progress, but spoils also the Console
registerDoParallel(cl) #  cluster: results by default are returned in a list
lisi.IDs <- foreach (i = 1:nrow(tt.united), .packages = c('doBy')) %dopar% {
	ID <- paste(tt.united[i, c("taxon", "group","longitude","latitude","year","month","day","depth")], sep="", collapse="_")
	print(paste(i))	
	return(ID)
}
stopCluster(cl)

# Prepare ID's new
vec.IDs <- c(unlist(lisi.IDs))
head(vec.IDs)
tt.final <- tt.united
tt.final$ID.new <- vec.IDs

# Save
fln <- "./Data_intermediate_steps/total_keys.RData"
save(tt.final, file = fln)
dim(tt.final) # 2'490'688 x 29

